import React ,{Component} from 'react'
import styles from '../../App.css'
import Settings from './settings'
import {Icon,Row,Col,Button,Dropdown,Menu} from 'antd'
import {connect} from 'react-redux'
import logo from '../../assets/bondira-logo.png'
import firebase from 'firebase'
import {browserHistory} from 'react-router'
const mapStateToProps =(state)=>({
        user:state.authReducer.user
})
class Header extends Component{
    logout(){
        firebase.auth().signOut().then(()=>browserHistory.push("/"))
    }
    render(){console.log(this.props.user,"@@ user")
        const menu = 
        <Menu>
            {firebase.auth().currentUser && 
            <Menu.Item>
                <span onClick={this.logout.bind(this)}>Logout</span>
            </Menu.Item>}
            <Menu.Item>
                hello
            </Menu.Item>
            <Menu.Item>
                hey
            </Menu.Item>
        </Menu>
        return(
            <div className="App-header">
                <Row>
                    <Col span={4}>
                    <Dropdown overlay={menu} placement="bottomRight" trigger={['click']}>
                    <Button style={{backgroundColor:"#272727",borderWidth:0}}>
                    <Icon type="bars" style={{ fontSize: 25, color: 'white' }} />
                    </Button>
                    </Dropdown>
                    </Col>
                    <Col span={20}>
                        <div className="Header-content" onClick={()=>browserHistory.push("/")}>
                            Bondira
                <img src={logo} height="80vh" width="70vw" />

                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default connect(mapStateToProps)(Header)