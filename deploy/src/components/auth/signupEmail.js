import React ,{Component} from 'react'
import { Form, Icon, Input, Button,Radio, Checkbox } from 'antd';
import styles from '../../styles/signupEmail.css';
import {browserHistory} from 'react-router'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux';
import {signup} from '../../store/middlewares/authMiddleware'
const FormItem = Form.Item;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;
const mapStateToProps = () =>({

})

const mapDispatchToProps = (dispatch)=>{console.log(dispatch)
  return bindActionCreators({
    signup
  },dispatch
)
}

class SignUpEmail extends React.Component {
  constructor(){
    super()
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        this.props.signup(values.name,values.email,values.password,values.userType)
      }
    });
  }
  render() {console.log("this.props",this.props)
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
            <FormItem>
              {getFieldDecorator('name', {
                rules: [{ required: true, message: 'Please input your Full Name!' }],
              })(
                <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} type="text"  placeholder="Full Name" 
                style={{color:"white"}}
                />
              )}
            </FormItem>
        <FormItem>
          {getFieldDecorator('email', {
            rules: [{ required: true, message: 'Please input your email!' }],
          })(
            <Input prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} type="email" placeholder="Email" 
            style={{color:"white"}}
            />
          )}
        </FormItem>
        <FormItem>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Please input your Password!' }],
          })(
            <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
            type="password" placeholder="Password" 
            />
          )}
        </FormItem>
        <FormItem
          // {...formItemLayout}
          // label="Please Select"
        >
          {getFieldDecorator('userType'
          , {
              rules: [{ required: true, message: 'يرجى تحديد نوع العمل' }],
            })(
            <RadioGroup style={{ color: "white" }}>
                <Radio checked={true} value="trucker"><span style={{ color: "white", fontSize: "1.2em" }}>(أنا أمتلك أو أقود أي من سيارات النقل (توكتوك ،ربع نقل ،نقل ثقيل ،جامبو</span></Radio><br/>
                <Radio value="rider"><span style={{ color: "white", fontSize: "1.2em" }}>أحتاج إلى استئجار سيارة نقل</span></Radio>
            </RadioGroup>
          )}
        </FormItem>
        <FormItem>
          <Button ghost htmlType="submit" className="login-form-button">
            تسجيل
          </Button>
          <br/><br/>
          <br/>------أو-------<br/>

          <a className="a" onClick={(ev)=>{
            browserHistory.push("/loginEmail");
            ev.preventDefault()
            }}>تسجيل الدخول الآن - هنا!</a>
        </FormItem>
      </Form>
    );
  }
}

const SignUpWithEmail = Form.create()(SignUpEmail);
export default connect(mapStateToProps,mapDispatchToProps)(SignUpWithEmail)