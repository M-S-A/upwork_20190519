import React, { Component } from 'react'
import { Rate, Form, Button, Input } from 'antd'
import firebase from 'firebase'
import {browserHistory} from 'react-router'
const FormItem = Form.Item
class Rating extends Component {
    constructor(props) {
        super(props);
    }
    rateTheRider(value) {
        firebase.database().ref("rating").child(this.props.routeParams.id).push({ [this.state.rating]: this.state.comments })
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        console.log(this.props, "check props")
        return (
            <Form onSubmit={
                (e) => {
                    e.preventDefault();
                    this.props.form.validateFields((err, values) => {
                        if (!err) {
                            firebase.database().ref("rating").child(this.props.id).once("value",(snap)=>{
                                console.log(snap.val(),"Rating check")
                                var DataSnap = snap.val()?snap.val().concat(values.rating):[values.rating]
                                console.log(DataSnap,"this is data snap")
                                console.log('Received values of form: ', values);
                                firebase.database().ref("rating")
                                .update(
                                        {[this.props.id]:DataSnap}
                                );
                                firebase.database().ref("user").child(firebase.auth().currentUser.uid).child("businessDetail")
                                .child("name").once("value",(snap)=>{
                                    var name=snap.val();
                                    firebase.database().ref("user").child(this.props.id).child("comments")
                                    .once("value",(snap)=>{
                                        var DataSnap = snap.val()?snap.val().concat({[name]:values.comments}):[{[name]:values.comments}];
                                        firebase.database().ref("user").child(this.props.id).update(
                                            JSON.parse(JSON.stringify({"comments":DataSnap}))
                                        )
                                    })
                                })



                                this.props.form.resetFields();
                                firebase.database().ref("user")
                                .child(firebase.auth().currentUser.uid).child("type").once("value",
                                (snap)=>{
                                    browserHistory.push( `/${snap.val()}`)
                                }
                            ).then(()=>console.log("done rating"))
                        })

                        }
                    });
                }
            }>
                <FormItem>
                    {getFieldDecorator('rating', { rules: [{ required: true }] })(
                        <Rate />
                    )}
                </FormItem>
                <FormItem
                    label={(
                        <span style={{ fontStyle: "italic", color: "white" }}>
                            Comments
                        </span>
                    )}
                >
                    {getFieldDecorator('comments', {
                        rules: [{ message: 'Please submit your comments' }],
                    })(
                        <Input.TextArea
                            style={{ borderRadius: 25, textAlign: "right", backgroundColor: "#626268", color: "white" }}
                            rows="7"
                            placeholder="Your Comments"
                        />)}
                </FormItem>
                <FormItem>
                    <Button
                        ghost
                        htmlType="submit"
                    >
                        Submit
          </Button>
                </FormItem>
            </Form>
        )
    }
}
export default Form.create()(Rating)