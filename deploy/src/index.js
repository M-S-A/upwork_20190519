import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import firebase from 'firebase'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

// Initialize Firebase
  var config = {
    apiKey: "SECRET_API_KEY",
    authDomain: "SECRET_AUTH_DOMAIN",
    databaseURL: "SECRET_DATABASE_URL",
    projectId: "SECRET_STORAGE_BUCKET",
    storageBucket: "SECRET_STORAGE_BUCKET_PROJECT_ID",
    messagingSenderId: "MESSAGING_SENDER_ID"
  };


firebase.initializeApp(config);


ReactDOM.render(
  <MuiThemeProvider>
<App />
</MuiThemeProvider>
, document.getElementById('root'));
registerServiceWorker();
