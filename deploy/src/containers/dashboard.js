import React ,{Component} from 'react';
import {Header} from '../components'
import {browserHistory} from 'react-router'
import {Button,Input,Affix,message} from 'antd'
import styles from '../styles/dashboard.css'
import style from '../App.css'
import firebase from 'firebase'
class Dashboard extends Component{
    constructor(){
        super();
        this.state={
            comments:""
        }
    }
componentDidMount(){
    firebase.auth().onAuthStateChanged((user)=>{
        if(user){
            console.log(user,"this is user",user.uid)
            firebase.database().ref("user").child(user.uid).once("value",(snap)=>
            {console.log(snap.val(),"this sympbol")
            var obj = snap.val();
            obj &&
            obj.businessDetail?
            browserHistory.push(`/${obj.type}`):
            browserHistory.push('/riderInfo ')
            }
           )
            
        }})
}
    render(){
        return(
            <div>
                <Affix>
                <Header/>
                </Affix>
            <div className="App" 
            >
                <div
                style={{
                    height:"100vh",
                    flex:1,
                    flexDirection:"row",
                    flexWrap:"wrap",
                    alignContent:"strech"
                    }}
                >
                <div className="gridDash">
                <Button ghost className="ghost" onClick={()=>browserHistory.push('/loginEmail')}>الدخول بالبريد الإلكتروني <br/> لدي حساب على بنديرة</Button>
                                <br/>
<br/>------أو-------<br/>
                <a onClick={()=>browserHistory.push('/registerEmail')}>التسجيل بالبريد الإلكتروني <br/> ليس لدي حساب على بنديرة</a>
                </div>
                <div className="gridDash">
                <Button ghost className="ghost" onClick={()=>browserHistory.push('/loginPhone')}>الدخول برقم الهاتف <br/> لدي حساب على بنديرة</Button>
                                <br/>
<br/>------أو-------<br/>
                <a onClick={()=>browserHistory.push('/registerPhone')}>التسجيل برقم الهاتف <br/> ليس لدي حساب على بنديرة</a>
                </div>
                <div className="comments" >
                
                <Input.TextArea onChange={(event)=>{this.setState({comments:event.target.value})}}
                style={{borderRadius:25,backgroundColor:"#626268",color:"white"}}
                rows="7"
                placeholder="أي اقتراحات أو شكاوى"
                value={this.state.comments}
                />
                <br/>
                <Button ghost id="submit"
                onClick={(event)=>{
                    event.preventDefault();
                    firebase.database().ref("suggestions").push(this.state.comments)
                    .then(()=>{this.setState({comments:""});message.success("شكرا لارسال ملاحظاتك")})
                }}
                >إرسال الاقتراحات أو الشكاوى</Button>
                </div>
            </div>
            </div>
            </div>
        )
    }
}
export default Dashboard