import React ,{Component} from 'react'
import {Header,SignInWithEmail} from '../components'

class LoginEmail extends Component{
    render(){
        return(
            <div
            style={{
                backgroundColor:"#41404c",
                height:"100vh"
            }}
            >
                <Header/>
                <br/>
                <SignInWithEmail/>
            </div>
        )
    }
}

export default LoginEmail