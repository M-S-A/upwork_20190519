import React,{Component} from 'react'
import {Header,SignInWithPhone} from '../components'
class LoginPhone extends Component{
    render(){
        return(
            <div
            style={{
                backgroundColor:"#41404c",
                height:"100vh"
            }}
            >
            <Header/>
            <br/>
            <SignInWithPhone/>
            </div>
        )
    }
}
export default LoginPhone