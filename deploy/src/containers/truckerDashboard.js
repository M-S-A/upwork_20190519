import React,{Component} from 'react';
import {Header,TruckerDashboard} from '../components'
import {Affix} from 'antd'
class Truck extends Component{
    render(){
        return(
            <div
                style={{
                    backgroundColor: "#41404c",
                    height: "100%",
                    minHeight:"100vh"
                }}
            >
                <Affix>
                    <Header />
                </Affix>
                {/* <br/> */}
                <TruckerDashboard />
            </div>
            
        )
    }
}
export default Truck